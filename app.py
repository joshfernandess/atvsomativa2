from flask import Flask
from flask import request

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def formulario():
    if request.method == 'POST':
        nome = request.form['nome']
        email = request.form['email']
        return f'Nome: {nome}<br>Email: {email}'
    
    return '''
        <!DOCTYPE html>
        <html>
        <head>
            <title>Formulário de Dados</title>
        </head>
        <body>
            <h1>Formulario de Dados</h1>
            <form method="POST">
                <label for="nome">Nome:</label>
                <input type="text" name="nome" required><br><br>

                <label for="email">Email:</label>
                <input type="email" name="email" required><br><br>

                <input type="submit" value="Enviar">
            </form>
        </body>
        </html>
    '''

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080, debug=True)
     #app.run(host="0.0.0.0", port=8080, debug=False)
